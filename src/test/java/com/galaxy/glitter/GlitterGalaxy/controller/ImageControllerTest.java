package com.galaxy.glitter.GlitterGalaxy.controller;

import com.google.common.io.Resources;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ImageControllerTest {
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Value("${product.imagePath}")
    String imagePath;

    @BeforeEach
    void setUp() throws IOException {
        mockMvc = MockMvcBuilders.
                webAppContextSetup(webApplicationContext)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();

        URL sourceResource = Resources.getResource("images/noimage.jpg");
        File sourceFile = new File(sourceResource.getFile());
        File destinationFile = new File(imagePath);

        FileUtils.copyFileToDirectory(sourceFile, destinationFile);

    }

    @AfterEach
    void tearDown() throws IOException {
        File destinationFile = new File(imagePath + "noimage.jpg");
        FileUtils.forceDelete(destinationFile);
    }

    @WithMockUser(value = "CustomUserDataDetailService")
    @Test
    public void shouldDownloadFile() throws Exception {
        //WHEN

        MvcResult result = mockMvc.perform(
                MockMvcRequestBuilders.get("/downloadFile/noimage.jpg").contentType(MediaType.APPLICATION_OCTET_STREAM))
                .andExpect(MockMvcResultMatchers.status().is(200)).andReturn();

        //THEN
         assertEquals(200, result.getResponse().getStatus());
    }
}