package com.galaxy.glitter.GlitterGalaxy.controller;

import com.galaxy.glitter.GlitterGalaxy.exception.ProductNotFoundException;
import com.galaxy.glitter.GlitterGalaxy.model.Product;
import com.galaxy.glitter.GlitterGalaxy.model.User;
import com.galaxy.glitter.GlitterGalaxy.repository.UserRepository;
import com.galaxy.glitter.GlitterGalaxy.service.CustomUserDataDetails;
import com.galaxy.glitter.GlitterGalaxy.service.ProductService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

//@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ProductControllerTest {

    private MockMvc mockMvc;

    @MockBean
    private ProductService productServiceMock;

    @MockBean
    private UserRepository userRepository;

    @Autowired
    private WebApplicationContext context;

    @BeforeEach
    void setUp()   {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

        User user = new User("user", "password", "user@gmail.com", "admin");
        CustomUserDataDetails customUserDataDetails = new CustomUserDataDetails(user);
        Mockito.when(userRepository.findByUserName("user")).thenReturn(user);
    }

//    @WithMockUser(value = "CustomUserDataDetailService")
//    @Test
//    public void shouldDisplayAllproducts() throws Exception {
//        Product product1 = new Product(1,"ToeRing", "ToeRing",
//                30, "IMG1.jpg",true);
//        Product product2 = new Product(2,"NoseRing", "NoseRing",
//                40, "IMG2.jpg",true);
//        Page<Product> productPage = new PageImpl(Arrays.asList(product1, product2));
//
//        when(productServiceMock.getAllProducts(any())).thenReturn(productPage);
//
//        mockMvc.perform(get("/"))
//                .andExpect(status().isOk())
//                .andExpect(view().name("index"))
//                .andExpect(model().attribute("products", productPage));
//
//        verify(productServiceMock, times(1)).getAllProducts(any());
//        assertEquals(productPage.getNumberOfElements(), 2);
//        assertFalse(productPage.hasPrevious());
//        assertFalse(productPage.hasNext());
//    }

    @Test
    public void shouldReturnProductDetailById() throws Exception {
        Product product1 = new Product(1, "ToeRing", "ToeRing",
                30, "IMG1.jpg",true);
        Optional<Product> optionalProduct = Optional.of(product1);

        when(productServiceMock.getProductById(1)).thenReturn(optionalProduct);

        mockMvc.perform(get("/products/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("product-detail"));

        verify(productServiceMock, times(1)).getProductById(1);

    }

    @Test
    public void shouldShowErrorPageIfProductIdInvalid() throws Exception {
        when(productServiceMock.getProductById(300)).thenThrow(new ProductNotFoundException());

        mockMvc.perform(get("/products/300"))
                .andExpect(status().isOk())
                .andExpect(view().name("error"))
                .andExpect(content().string(containsString("Product not found")));


        verify(productServiceMock, times(1)).getProductById(300);
        verifyNoMoreInteractions(productServiceMock);
    }

//    @WithMockUser(value = "CustomUserDataDetailService")
//    @Test
//    public void shouldAddProductImage() throws Exception {
//        //GIVEN
//        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", "excel.jpg", "multipart/form-data", "excel".getBytes());
//
//        //WHEN
//        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.fileUpload("/add")
//                .file(mockMultipartFile).contentType(MediaType.MULTIPART_FORM_DATA))
//                .andExpect(MockMvcResultMatchers.status().is(200)).andReturn();
//
//        //THEN
//        assertEquals(200, result.getResponse().getStatus());
//        assertNotNull(result.getResponse().getContentAsString());
//    }



//
////    @WithMockUser(username = "admin", roles = {"ADMIN"})
////@PreAuthorize("hasAuthority('USER')")
//    @WithMockUser(value = "spring")
//    @Test
//    public void shouldSaveProduct() throws Exception {
//
//        //GIVEN
//        Product product1 = new Product(1, "ToeRing", "ToeRing",
//                30, "IMG1.jpg");
//        //WHEN
//        MvcResult result = mockMvc.perform(post("/add", product1))
//                .andExpect(MockMvcResultMatchers.status().isOk())
//                .andReturn();
//
//        //THEN
//        Assert.assertEquals(200, result.getResponse().getStatus());
//        Assert.assertNotNull(result.getResponse().getContentAsString());
//
//
//        verify(productServiceMock, times(1)).saveProduct(product1);
//    }

////    @WithMockUser(username = "admin", roles = {"ADMIN"})
////@PreAuthorize("hasAuthority('USER')")
//@WithMockUser(value = "spring")
//    @Test
//    public void shouldUpdateProduct() throws Exception {
//
//        //GIVEN
//        Product product1 = new Product(1, "ToeRing", "ToeRing",
//                30, "IMG1.jpg");
//        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", "excel.jpg", "multipart/form-data", "excel".getBytes());
//        //WHEN
//        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.fileUpload("/update/1", product1)
//                .file(mockMultipartFile).contentType(MediaType.MULTIPART_FORM_DATA))
//                .andExpect(MockMvcResultMatchers.status().is(200)).andReturn();
//
//        //THEN
//        Assert.assertEquals(200, result.getResponse().getStatus());
//        Assert.assertNotNull(result.getResponse().getContentAsString());
//
//
//        verify(productServiceMock, times(1)).saveProduct(product1);
//    }
}