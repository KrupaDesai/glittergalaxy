package com.galaxy.glitter.GlitterGalaxy.service;

import com.galaxy.glitter.GlitterGalaxy.model.Product;
import com.galaxy.glitter.GlitterGalaxy.repository.ProductRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {

    @InjectMocks
    ProductService productserviceMock;

    @Mock
    ProductRepository productRepoMock;

    @Test
    public void shouldGetAllActiveProducts(){
        Product product1 = new Product(1,"ToeRing", "ToeRing",
                30, "IMG1.jpg",true);
        Product product2 = new Product(2,"NoseRing", "NoseRing",
                40, "IMG2.jpg",true);
        Page<Product> productPage = new PageImpl(Arrays.asList(product1,product2));

        given(productRepoMock.findAllAvailableProducts(PageRequest.of(0,1))).willReturn(productPage);

        Page<Product> productList = productserviceMock.getAllProducts(PageRequest.of(0,1));
        assertEquals(2, productList.getTotalElements());
    }

    @Test
    public void shouldGetProductDetailById(){
        Product product1 = new Product(1,"ToeRing", "ToeRing",
                30,"IMG1.jpg",true);
        Optional<Product> optionalProduct = Optional.of(product1);


        Mockito.when(productRepoMock.findById(1)).thenReturn(optionalProduct);
        Optional<Product> product = productserviceMock.getProductById(1);
        assertEquals(product1.getProductId(),product.get().getProductId());
    }

    @Test
    public void shouldSaveProduct(){
        //GIVEN
        Product product1 = new Product("ToeRing", "ToeRing",
                30,"IMG1.jpg",true);
        Optional<Product> optionalProduct = Optional.of(product1);

        //WHEN
        productserviceMock.saveProduct(product1);

        //THEN
        verify(productRepoMock, times(1)).save(product1);
    }




}