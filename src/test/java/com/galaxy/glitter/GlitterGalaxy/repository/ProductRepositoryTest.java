package com.galaxy.glitter.GlitterGalaxy.repository;

import com.galaxy.glitter.GlitterGalaxy.model.Product;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProductRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ProductRepository productRepository;


    @Test
    public void shouldFindAll(){
        Product product1 = new Product("BangleSet","2 Pieces of Bangles", 100,"img1.jpg",true);
        Product product2 = new Product("EarRings","Perl ear rings",60,"",true);
        entityManager.persist(product1);
        entityManager.persist(product2);
        Assertions.assertEquals(2,productRepository.findAllAvailableProducts(PageRequest.of(0,1)).getTotalElements());
    }

    @Test
    public void shouldGetProductById() {
        Product product1 = new Product("BangleSet","2 Pieces of Bangles", 100,"img1.jpg",true);
        entityManager.persist(product1);
        Optional<Product> product = Optional.of(product1);
        assertEquals(product.get().getProductDescription(), productRepository.findById(product1.getProductId()).get().getProductDescription());
        assertEquals(product.get().getProductName(), productRepository.findById(product1.getProductId()).get().getProductName());
    }

}