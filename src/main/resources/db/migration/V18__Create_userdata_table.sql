CREATE TABLE IF NOT EXISTS userdata
(
   id serial NOT NULL,
   name VARCHAR(10) NOT NULL,
   password VARCHAR(20) NOT NULL,
   role VARCHAR(10) NOT NULL,
   email VARCHAR(50) NOT NULL,
   CONSTRAINT userdata_pk PRIMARY KEY(id)

);