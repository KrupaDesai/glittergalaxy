CREATE TABLE IF NOT EXISTS images(
  image_id INTEGER NOT NULL,
  product_id INTEGER REFERENCES products(product_id),
  image_name VARCHAR (255),
  image_data VARCHAR (500)
);
