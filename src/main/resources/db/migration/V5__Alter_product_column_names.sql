ALTER TABLE products
RENAME COLUMN product_id TO id;

ALTER TABLE products
RENAME COLUMN product_name TO name;

ALTER TABLE products
RENAME COLUMN product_description TO description;

ALTER TABLE products
RENAME COLUMN product_price TO price;



