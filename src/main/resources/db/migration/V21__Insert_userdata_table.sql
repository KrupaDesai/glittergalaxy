INSERT INTO userdata (id, name, password, role, email)
    VALUES(105,'admin','$2a$10$kcayjvBNqNoHhbV2DZ8owOZSVwjofz5vLojbdFyN46f0Xlz8qbKOu','admin', 'ns@mail.com'),
                  (106,'customer','$2a$10$kcayjvBNqNoHhbV2DZ8owOZSVwjofz5vLojbdFyN46f0Xlz8qbKOu','customer','kj@gmail.com'),
              (107,'prachi','$2a$10$kcayjvBNqNoHhbV2DZ8owOZSVwjofz5vLojbdFyN46f0Xlz8qbKOu','admin','kd@gmail.com'),
              (108,'prachiv','$2a$10$kcayjvBNqNoHhbV2DZ8owOZSVwjofz5vLojbdFyN46f0Xlz8qbKOu','customer','pv@gmail.com')