CREATE TABLE products
(
    product_id bigint NOT NULL,
    product_name character varying(100) NOT NULL,
    product_description character varying(300),
    product_price double precision,
    CONSTRAINT product_pkey PRIMARY KEY (product_id)
)