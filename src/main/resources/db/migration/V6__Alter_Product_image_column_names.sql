ALTER TABLE images
RENAME COLUMN image_id TO id;

ALTER TABLE images
RENAME COLUMN image_name TO name;

ALTER TABLE images
RENAME COLUMN image_data TO url;



