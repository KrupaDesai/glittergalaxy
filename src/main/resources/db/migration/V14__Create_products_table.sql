CREATE TABLE products
(
    id serial NOT NULL ,
    name character varying(255)  NOT NULL,
    description character varying(500) ,
    price double precision NOT NULL,
    image_url character varying(250) ,
    PRIMARY KEY (id)
)