package com.galaxy.glitter.GlitterGalaxy.controller;

import com.galaxy.glitter.GlitterGalaxy.exception.ProductNotFoundException;
import com.galaxy.glitter.GlitterGalaxy.model.Product;
import com.galaxy.glitter.GlitterGalaxy.service.ProductService;
import com.galaxy.glitter.GlitterGalaxy.util.Pager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;


@Controller

public class ProductController {
    private static final int INITIAL_PAGE = 0;
    @Value("${product.imagePath}")
    private String uploadDir;


    @Autowired
    ProductService productService;

    @GetMapping("/")
    public String getAllProducts(Model model,@RequestParam("page") Optional<Integer> page){
        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
        redirectToListPage(model, evalPage);
        return "index";
    }

    @GetMapping("/products/{id}")
    public String getProductFromId(@PathVariable("id") int id, Model model) {
        Product product = productService.getProductById(id).orElseThrow(() -> new ProductNotFoundException());
        model.addAttribute("product", product);
        return "product-detail";
    }

    @GetMapping("/add")
    public String getAddProduct(@Valid Product product, BindingResult result, Model model) {

        return "add-product";
    }

    @PostMapping("/add")
    public String addProduct(@RequestParam("file") MultipartFile file, @Valid Product product, BindingResult result, Model model) throws IOException {
        if (result.hasErrors()) {
            return "add-product";
        }
        if(!file.isEmpty()) {
            byte[] bytes = file.getBytes();
            Path path = Paths.get( uploadDir+ file.getOriginalFilename());
            Files.write(path, bytes);
            product.setProductImageUrl(file.getOriginalFilename());
        }
        productService.saveProduct(product);
        return "redirect:/";
    }

    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") int id, Model model) {
        Product product = productService.getProductById(id)
                .orElseThrow(() -> new ProductNotFoundException());
        model.addAttribute("product", product);
        return "update-product";
    }

    @PostMapping("/update/{id}")
    public String updateProduct(@RequestParam("file") MultipartFile file,@PathVariable("id") int id, @Valid Product product, BindingResult result,
                                Model model) throws IOException {
        if (result.hasErrors()) {
            product.setProductId(id);
            return "update-product";
        }
        Optional<Product> productToUpdate = productService.getProductById(id);
        productToUpdate.get().setProductName(product.getProductName());
        productToUpdate.get().setProductDescription(product.getProductDescription());
        productToUpdate.get().setProductPrice(product.getProductPrice());
        if(!file.isEmpty()) {
            byte[] bytes = file.getBytes();
            Path path = Paths.get( uploadDir+ file.getOriginalFilename());
            Files.write(path, bytes);
            productToUpdate.get().setProductImageUrl(file.getOriginalFilename());
        }

        productService.saveProduct(productToUpdate.get());

        redirectToListPage(model, 0);
        return "index";
    }

    @GetMapping("/delete/{id}")
    public String getProduct(@PathVariable("id") int id, Model model) {
        Product productToDelete = productService.getProductById(id)
                .orElseThrow(() ->  new ProductNotFoundException());
        productToDelete.setProductAvailable(false);
        productService.saveProduct(productToDelete);
        redirectToListPage(model, 0);
        return "index";
    }

    private void redirectToListPage(Model model, int evalPage) {
        Page<Product> products = productService.getAllProducts(PageRequest.of(evalPage, 3));
        Pager pager = new Pager(products);

        model.addAttribute("pager", pager);
        model.addAttribute("products", products);
    }
}
