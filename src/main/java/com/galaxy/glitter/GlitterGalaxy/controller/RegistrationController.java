package com.galaxy.glitter.GlitterGalaxy.controller;

import com.galaxy.glitter.GlitterGalaxy.model.User;
import com.galaxy.glitter.GlitterGalaxy.model.UserForm;
import com.galaxy.glitter.GlitterGalaxy.repository.UserRepository;
import com.galaxy.glitter.GlitterGalaxy.service.CustomUserDataDetailService;
import com.galaxy.glitter.GlitterGalaxy.service.UserValidatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class RegistrationController {
    @Autowired
    private UserRepository userRepository;


    @Autowired
    private UserValidatorService userValidatorService;

    @Autowired
    private CustomUserDataDetailService customUserDataDetailService;


    @InitBinder("personForm")
    protected void initUserFormBinder(WebDataBinder binder) {
        binder.addValidators(userValidatorService);
    }

    @GetMapping("/registration")
    public String showRegistration(@ModelAttribute UserForm userForm,BindingResult bindingResult,Model model) {

        return "registration";
    }


    @PostMapping("/registration")
    public String saveRegistration(@Validated @ModelAttribute UserForm userForm, BindingResult bindingResult, Model model ,final RedirectAttributes redirectAttributes) {
        userValidatorService.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            System.out.println(bindingResult.getAllErrors().size());
            return "registration";
        }

        User existing = userRepository.findByUserName(userForm.getUserName());
        if (existing != null) {
            bindingResult
                    .rejectValue("userName", null,
                            "There is already an account registered with that name");
            return "registration";
        }

        customUserDataDetailService.save(userForm);



        return "redirect:/login";
    }
}
