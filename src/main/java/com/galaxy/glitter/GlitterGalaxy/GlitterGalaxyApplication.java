package com.galaxy.glitter.GlitterGalaxy;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;


@SpringBootApplication
public class GlitterGalaxyApplication {


    public static void main(String[] args) {


        ConfigurableApplicationContext ctx =
                SpringApplication.run(GlitterGalaxyApplication.class, args);

    }

}
