package com.galaxy.glitter.GlitterGalaxy.service;

import com.galaxy.glitter.GlitterGalaxy.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CustomUserDataDetails extends User implements UserDetails {
    private static final long serialVersionUID = 1L;
    private List<String> userRole = new ArrayList<>();

    public CustomUserDataDetails(User user) {
        super(user);
        this.userRole.add(user.getUserRole());
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        String role = StringUtils.collectionToCommaDelimitedString(userRole);
        return AuthorityUtils.commaSeparatedStringToAuthorityList(role);
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String getUsername(){
        return super.getUserName();
    }

}
