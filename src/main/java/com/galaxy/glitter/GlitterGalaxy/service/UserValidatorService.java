package com.galaxy.glitter.GlitterGalaxy.service;

import com.galaxy.glitter.GlitterGalaxy.model.User;
import com.galaxy.glitter.GlitterGalaxy.model.UserForm;
import com.galaxy.glitter.GlitterGalaxy.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class UserValidatorService implements Validator {



        @Autowired
        private UserRepository userRepository;

        @Override
        public boolean supports(Class<?> aClass) {
            return User.class.equals(aClass);
        }

        @Override
        public void validate(Object o, Errors errors) {
            UserForm userForm = (UserForm) o;

            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userName", "NotEmpty");
            if (userForm.getUserName().length() < 6 || userForm.getUserName().length() > 32) {
                errors.rejectValue("userName", "Size.userForm.userName");
            }
            if (userRepository.findByUserName(userForm.getUserName()) != null) {
                errors.rejectValue("userName", "Duplicate.userForm.userName");
            }

            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty");
            if (userForm.getPassword().length() < 8 || userForm.getPassword().length() > 32) {
                errors.rejectValue("password", "Size.userForm.password");
            }

            if (!userForm.getConfirmPassword().equals(userForm.getPassword())) {
                errors.rejectValue("confirmPassword", "Diff.userForm.confirmPassword");
            }

            if(!userForm.getEmailId().contains("@")){
                errors.rejectValue("emailId", "invalid email id");
            }
        }
    }

