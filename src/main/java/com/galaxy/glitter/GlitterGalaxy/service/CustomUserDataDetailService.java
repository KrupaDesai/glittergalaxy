package com.galaxy.glitter.GlitterGalaxy.service;

import com.galaxy.glitter.GlitterGalaxy.model.User;
import com.galaxy.glitter.GlitterGalaxy.model.UserForm;
import com.galaxy.glitter.GlitterGalaxy.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDataDetailService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    public void save(UserForm userForm) {
        User user1 = new User();
        user1.setUserName(userForm.getUserName());
        user1.setEmailId(userForm.getEmailId());
        user1.setUserRole(userForm.getUserRole());
        user1.setPassword(passwordEncoder.encode(userForm.getPassword()));
        userRepository.save(user1);
    }

    @Autowired
    public CustomUserDataDetailService(UserRepository userDataRepo){
        this.userRepository = userDataRepo;
}

    @Override
    public UserDetails loadUserByUsername(String username) {
        if (username.trim().isEmpty()) {
            throw new UsernameNotFoundException("No user present with the username" + username);
        }

        User user = userRepository.findByUserName(username);
        if(null == user)
            throw new UsernameNotFoundException("No user present with the username" + username);
        return new CustomUserDataDetails(user);
    }


}
