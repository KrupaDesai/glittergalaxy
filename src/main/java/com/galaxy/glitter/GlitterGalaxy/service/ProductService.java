package com.galaxy.glitter.GlitterGalaxy.service;


import com.galaxy.glitter.GlitterGalaxy.model.Product;
import com.galaxy.glitter.GlitterGalaxy.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public Optional<Product> getProductById(int id){
        return productRepository.findById(id);
    }

    public Page<Product> getAllProducts(Pageable pageable){
        return productRepository.findAllAvailableProducts(pageable);
    }

    public void saveProduct(Product product) {
         productRepository.save(product);
    }
}
