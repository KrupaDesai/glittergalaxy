package com.galaxy.glitter.GlitterGalaxy.exception;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ProductExceptionController {
    @ExceptionHandler(value = ProductNotFoundException.class)
    public String exception(ProductNotFoundException exception, Model model) {
       // return new ResponseEntity<>("Product not found", HttpStatus.NOT_FOUND);
        model.addAttribute("errorMessage","Product not found");
        return "error";
    }
}
