package com.galaxy.glitter.GlitterGalaxy.repository;

import com.galaxy.glitter.GlitterGalaxy.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
  User findByUserName(String username);
}
