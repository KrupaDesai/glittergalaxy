package com.galaxy.glitter.GlitterGalaxy.repository;

import com.galaxy.glitter.GlitterGalaxy.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface ProductRepository  extends JpaRepository<Product,Integer> {
    @Query(value = "SELECT * FROM products p where p.available=true", nativeQuery = true)
    Page<Product> findAllAvailableProducts(Pageable pageable);
}
