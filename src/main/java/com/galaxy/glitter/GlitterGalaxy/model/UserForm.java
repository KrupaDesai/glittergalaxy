package com.galaxy.glitter.GlitterGalaxy.model;

public class UserForm {
    private String userName;
    private String password;
    private String confirmPassword;
    private String emailId;
    private String userRole;

    public UserForm() {
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public String getEmailId() {
        return emailId;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }
}
