package com.galaxy.glitter.GlitterGalaxy.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "userdata")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int userId;

    @NotNull
    @Column(name = "name")
    private String userName;

    @NotNull
    @Column(name = "password")
    private String password;

    @Transient
    private String passwordConfirm;;

    @NotNull
    @Column(name = "email")
    private String emailId;


    @Column(name = "role")
    private String userRole;

    public User(int userId, @NotNull String userName, @NotNull String password, @NotNull String emailId, @NotNull String userRole) {
        this.userId = userId;
        this.userName = userName;
        this.password = password;
        this.emailId = emailId;
        this.userRole = userRole;
    }

    public User(User newuser) {
        this.userId = newuser.userId;
        this.userName = newuser.userName;
        this.password = newuser.password;
        this.emailId = newuser.emailId;
        this.userRole = newuser.userRole;
    }

    public User() {
    }

    public User(@NotNull String userName, @NotNull String password, @NotNull String emailId, @NotNull String userRole) {
        this.userName = userName;
        this.password = password;
        this.emailId = emailId;
        this.userRole = userRole;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }
    @Transient
    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }
}
